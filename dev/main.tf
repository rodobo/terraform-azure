provider "azurerm" {
  subscription_id  = "${var.azure_id}"
}

# resource group
resource "azurerm_resource_group" "web" {
  name = "${var.azure_resource_group}"
  location = "${var.azure_location}"
}

# virtual network
resource "azurerm_virtual_network" "vnet" {
  name                = "${var.virtual_network_name}"
  location            = "${azurerm_resource_group.web.location}"
  address_space       = ["${var.address_space}"]
  resource_group_name = "${azurerm_resource_group.web.name}"
}

# network interface
resource "azurerm_network_interface" "magnitude-nic" {
  name                      = "${var.prefix}magnitude-nic"
  location                  = "${var.azure_location}"
  resource_group_name       = "${azurerm_resource_group.web.name}"
  network_security_group_id = "${azurerm_network_security_group.magnitude-sg.id}"

  ip_configuration {
    name                          = "${var.prefix}ipconfig"
    subnet_id                     = "${azurerm_subnet.default.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.magnitude-pip.id}"
  }
}

# public IP
resource "azurerm_public_ip" "magnitude-pip" {
  name                         = "${var.prefix}-ip"
  location                     = "${var.azure_location}"
  resource_group_name          = "${azurerm_resource_group.web.name}"
  allocation_method = "Dynamic"
  domain_name_label            = "${var.hostname}"
}

# subnet
resource "azurerm_subnet" "default" {
  name                 = "${var.prefix}subnet"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  resource_group_name  = "${azurerm_resource_group.web.name}"
  address_prefix       = "${var.subnet_prefix}"
}

# Security group to allow inbound access on port 80 (http) and 22 (ssh)
resource "azurerm_network_security_group" "magnitude-sg" {
  name                = "${var.prefix}-sg"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.web.name}"

  security_rule {
    name                       = "HTTP"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "${var.source_network}"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "SSH"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "${var.source_network}"
    destination_address_prefix = "*"
  }
}

# virtual machine instance
resource "azurerm_virtual_machine" "site" {
  name                = "${var.hostname}-site"
  location            = "${var.azure_location}"
  resource_group_name = "${azurerm_resource_group.web.name}"
  vm_size             = "${var.vm_size}"

  network_interface_ids         = ["${azurerm_network_interface.magnitude-nic.id}"]
  delete_os_disk_on_termination = "true"

  storage_image_reference {
    publisher = "${var.image_publisher}"
    offer     = "${var.image_offer}"
    sku       = "${var.image_sku}"
    version   = "${var.image_version}"
  }

  storage_os_disk {
    name              = "${var.hostname}-osdisk"
    managed_disk_type = "Standard_LRS"
    caching           = "ReadWrite"
    create_option     = "FromImage"
  }

  os_profile {
    computer_name  = "${var.hostname}"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  # file provisioner to copy app files dir to server
  provisioner "file" {
    source      = "${var.path_to_app}/${var.app_dir_name}"
    destination = "./"
  }

  # remote provisioner to install nginx, install app dependencies and run app
  provisioner "remote-exec" {
    inline = [
      "sudo apt -y update",
      "sudo apt -y install nginx",
      "sudo service nginx start",
      "curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -",
      "echo \"deb https://dl.yarnpkg.com/debian/ stable main\" | sudo tee /etc/apt/sources.list.d/yarn.list",
      "sudo apt -y install yarn",
      "mkdir ${var.app_dir_name}",
      "cd ${var.app_dir_name}",
      "yarn install",
      "yarn start"
    ]

    connection {
      type     = "ssh"
      user     = "${var.admin_username}"
      password = "${var.admin_password}"
      host     = "${azurerm_public_ip.magnitude-pip.fqdn}"
    }
  }
}
